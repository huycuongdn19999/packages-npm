/**
 * Interface defining the component's options object
 */
export interface VasDataWarningLevelComponentOptions {
  // Add the definitions here
}

/**
 * Default options for the component
 */
export const DEFAULT_VAS_DATA_WARNING_LEVEL_OPTIONS: VasDataWarningLevelComponentOptions = {
  // Specify the values here
};
export type MeasuringLogValueType = {
  value?: number;
  unit?: string;
  approvedValue?: number;
  statusDevice?: 0 | 1 | 2 | 3;
  warningLevel?: 'GOOD' | 'EXCEEDED' | 'EXCEEDED_PREPARING';
  minLimit?: number | '' | null;
  maxLimit?: number | '' | null;
  minTend?: number | '' | null;
  maxTend?: number | '' | null;
  minRange?: number | '' | null;
  maxRange?: number | '' | null;
};
