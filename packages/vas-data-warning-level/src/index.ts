import {MeasuringLogValueType} from './types';
import _ from 'lodash';


export * from './types';
/**
 * chuyển toàn bộ các giá trị rỗng, undefined "" thành null để xử lý
 */
const standardExceedConfig = ({
  minRange,
  maxRange,
  minTend,
  maxTend,
  minLimit,
  maxLimit,
  value,
}: MeasuringLogValueType): MeasuringLogValueType => {
  const clone = {
    minRange,
    maxRange,
    minTend,
    maxTend,
    minLimit,
    maxLimit,
    value,
  };
  try {
    // console.log(clone, 'clonecloneclone');

    if (minRange === '' || minRange === undefined) {
      _.set(clone, 'minRange', null);
    }
    if (maxRange === '' || maxRange === undefined) {
      _.set(clone, 'maxRange', null);
    }

    if (minTend === '' || minTend === undefined) {
      _.set(clone, 'minTend', null);
    }
    if (maxTend === '' || maxTend === undefined) {
      _.set(clone, 'maxTend', null);
    }

    if (minLimit === '' || minLimit === undefined) {
      _.set(clone, 'minLimit', null);
    }
    if (maxLimit === '' || maxLimit === undefined) {
      _.set(clone, 'maxLimit', null);
    }
    return clone;
  } catch (error) {
    console.log(error, 'errorerror');
  }
  return clone;
};

const isValueGood = (measureLog: MeasuringLogValueType): boolean => {
  const meaLogClone = standardExceedConfig(measureLog);
  const {minLimit, maxLimit, minTend, maxTend, value} = meaLogClone;
  // console.log(measureLog, '==measureLog==');
  // debug('isValueGood => ');
  // debug({minLimit, maxLimit, minTend, maxTend, value});

  if (
    minLimit !== null &&
    maxLimit !== null &&
    minTend !== null &&
    maxTend !== null &&
    value! >= minTend! &&
    value! <= maxTend!
  ) {
    return true;
  }
  if (
    minLimit === null &&
    maxLimit === null &&
    minTend === null &&
    maxTend === null
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit !== null &&
    value! >= minLimit! &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit !== null &&
    value! >= minTend! &&
    value! <= maxLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit !== null &&
    value! >= minTend! &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit === null &&
    value! >= minTend! &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit !== null &&
    value! >= minLimit! &&
    value! <= maxLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit !== null &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit === null &&
    value! <= minLimit! &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit !== null &&
    value! >= minTend! &&
    value! <= maxLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit === null &&
    value! >= minTend!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit === null &&
    value! >= minTend! &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit !== null &&
    value! <= maxLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit === null &&
    value! >= minLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit === null &&
    value! <= maxTend!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit === null &&
    value! >= minTend!
  ) {
    return true;
  }

  return false;
};
const isValueExceeded = (measureLog: MeasuringLogValueType): boolean => {
  const meaLogClone = standardExceedConfig(measureLog);

  const {minLimit, maxLimit, minTend, maxTend, value} = meaLogClone;

  if (
    minLimit === null &&
    maxLimit === null &&
    minTend === null &&
    maxTend === null
  ) {
    return false;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit !== null &&
    (value! > maxLimit! || value! < minLimit!)
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit !== null &&
    (value! > maxLimit! || value! < minLimit!)
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit !== null &&
    value! > maxLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit === null &&
    value! < minLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit !== null &&
    (value! > maxLimit! || value! < minLimit!)
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit !== null &&
    value! > maxLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit === null &&
    value! < minLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit !== null &&
    value! > maxLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit === null &&
    value! < minLimit!
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit === null
  ) {
    return false;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit !== null &&
    value! > maxLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend === null &&
    minLimit !== null &&
    maxLimit === null &&
    value! < minLimit!
  ) {
    return true;
  }

  if (
    minTend === null &&
    maxTend !== null &&
    minLimit === null &&
    maxLimit === null
  ) {
    return false;
  }

  if (
    minTend !== null &&
    maxTend === null &&
    minLimit === null &&
    maxLimit === null
  ) {
    return true;
  }

  if (
    minTend !== null &&
    maxTend !== null &&
    minLimit !== null &&
    maxLimit !== null &&
    (value! < minLimit! || value! > maxLimit!)
  ) {
    return true;
  }

  return false;
};

const replaceMeasuringLog = (
  measureLog: MeasuringLogValueType,
): MeasuringLogValueType => {
  let measureLogClone = _.clone(measureLog);
  measureLogClone = standardExceedConfig(measureLogClone);
  // const {value} = measureLogClone;

  const {minTend, maxTend, minLimit, maxLimit, value} = measureLogClone;
  // console.log(measureLogClone, '---------measureLogClone');
  measureLogClone.warningLevel = 'GOOD';

  const isGood = isValueGood({
    minLimit,
    maxLimit,
    minTend,
    maxTend,
    value,
  });

  const isExceeded = isValueExceeded({
    minLimit,
    maxLimit,
    minTend,
    maxTend,
    value,
  });

  if (isGood) {
    measureLogClone.warningLevel = 'GOOD';
  } else if (isExceeded) {
    measureLogClone.warningLevel = 'EXCEEDED';
  } else {
    measureLogClone.warningLevel = 'EXCEEDED_PREPARING';
  }

  return measureLogClone;
};
export {isValueGood, isValueExceeded, replaceMeasuringLog};
