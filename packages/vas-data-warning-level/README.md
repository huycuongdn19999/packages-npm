# vas-data-warning-level

# build

[![LoopBack](<https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png>)](http://loopback.io/)

## Installation

Install VasDataWarningLevelComponent using `npm`;

```sh
$ npm install vas-data-warning-level
```

## Basic Use

Configure and load VasDataWarningLevelComponent in the application constructor
as shown below.

```ts
import {VasDataWarningLevelComponent, VasDataWarningLevelComponentOptions, DEFAULT_VAS_DATA_WARNING_LEVEL_OPTIONS} from 'vas-data-warning-level';
// ...
export class MyApplication extends BootMixin(ServiceMixin(RepositoryMixin(RestApplication))) {
  constructor(options: ApplicationConfig = {}) {
    const opts: VasDataWarningLevelComponentOptions = DEFAULT_VAS_DATA_WARNING_LEVEL_OPTIONS;
    this.configure(VasDataWarningLevelComponentBindings.COMPONENT).to(opts);
      // Put the configuration options here
    });
    this.component(VasDataWarningLevelComponent);
    // ...
  }
  // ...
}
```
